
var desktopsArray = desktopsForActivity(currentActivity());
for (var j = 0; j < desktopsArray.length; j++) {
    var desk = desktopsArray[j];
    desk.wallpaperPlugin = "org.kde.slideshow";
    desk.addWidget("org.kde.plasma.digitalclock",
                   2 * gridUnit,
                   2 * gridUnit,
                   15 * gridUnit,
                   8 * gridUnit);

//    desk.addWidget("org.kde.plasma.mycroftplasmoid");

    desk.currentConfigGroup = new Array("Wallpaper","org.kde.slideshow","General");
    desk.writeConfig("SlideInterval", 480);
    desk.writeConfig("SlidePaths", "/usr/share/wallpapers/");
}

var panel = new Panel("org.kde.mycroft.tablet.panel")
panel.location = "top";
panel.height = 2 * gridUnit;

panel.addWidget("org.kde.plasma.notifications");

panel.addWidget("org.kde.plasma.devicenotifier");
var battery = panel.addWidget("org.kde.plasma.battery");
battery.currentConfigGroup = ["Configuration", "General"]
battery.writeConfig("showPercentage", true)
battery.reloadConfig()

panel.addWidget("org.kde.plasma.networkmanagement");

panel.addWidget("org.kde.plasma.digitalclock");


var screenGeom = screenGeometry(0);

var launcher = desk.addWidget("org.kde.mycroft.tablet.launcher");
launcher.locked = true;


var panel = new Panel("org.kde.mycroft.tablet.taskpanel")
panel.location = "bottom";
panel.hiding = "windowsbelow";
panel.height = gridUnit;
