/*
 *   Copyright 2015 Marco Martin <notmart@gmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.4
import QtQuick.Window 2.2
import QtQuick.Controls 2.3 as Controls
import org.kde.taskmanager 0.1 as TaskManager
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Controls.Control {
    id: delegate
    width: height * 1.6
    height: parent.height
    opacity: 1 + y/height

    background: Rectangle {
        color: model.IsActive ? theme.highlightColor : theme.backgroundColor
    }
    contentItem: MouseArea {
        implicitWidth: layout.implicitWidth
        implicitHeight: layout.implicitHeight
        drag {
            target: model.IsClosable ? delegate : null
            axis: Drag.YAxis
            maximumY: 0
        }
        onPressed: {
            drawer.closePolicy = Controls.Popup.CloseOnEscape 
        }
        onReleased: {
            drawer.closePolicy = Controls.Popup.CloseOnEscape | Controls.Popup.CloseOnReleaseOutside;
            if (delegate.y < -delegate.height/2) {
                tasksModel.requestClose(tasksModel.index(model.index, 0));
            } else {
                restoreAnim.restart();
            }
        }
        onCanceled: {
            drawer.closePolicy = Controls.Popup.CloseOnEscape | Controls.Popup.CloseOnReleaseOutside;
        }
        onClicked: {
            if (delegate.y < -units.gridUnit) {
                return;
            }
            window.close();
            window.setSingleActiveWindow(model.index);
        }
        YAnimator {
            id: restoreAnim
            target: delegate
            from: delegate.y
            to: 0
            duration: units.longDuration
            easing.type: Easing.InOutQuad
        }
        ColumnLayout {
            id: layout
            anchors.fill: parent
            Item {
                Layout.fillWidth: true
                implicitHeight: parent.height - label.implicitHeight
                PlasmaCore.IconItem {
                    anchors.fill: parent
                    visible: !thumbnail.thumbnailAvailable
                    source: model.decoration
                }
                PlasmaCore.WindowThumbnail {
                    id: thumbnail
                    anchors.fill: parent
                    visible: thumbnailAvailable
                    winId: model.WinIdList[0]
                }
            }
            PlasmaComponents.Label {
                id: label
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignBottom
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideRight
                text: model.display
            }
        }
    }
}

