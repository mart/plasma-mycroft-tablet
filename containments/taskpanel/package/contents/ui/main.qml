/*
 *  Copyright 2015 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.taskmanager 0.1 as TaskManager
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kquickcontrolsaddons 2.0

PlasmaCore.ColorScope {
    id: root
    width: 600
    height: 480

    colorGroup: PlasmaCore.Theme.ComplementaryColorGroup
    property Item toolBox

    Plasmoid.backgroundHints: plasmoid.configuration.PanelButtonsVisible ? PlasmaCore.Types.StandardBackground : PlasmaCore.Types.NoBackground

    TaskSwitcher {
        id: taskSwitcher
    }

    //HACK: libtaskmanager bug?
    Component.onCompleted: tasksModel.countChanged()

    MouseArea {
        id: mainMouseArea
        anchors.fill: parent
        property int oldMouseY: 0
        property int startMouseY: 0
        property bool isDragging: false
        property bool wasShowingDesktop
        drag.filterChildren: true
        function managePressed(mouse) {
            wasShowingDesktop = plasmoid.nativeInterface.showDesktop;
            startMouseY = oldMouseY = mouse.y;
            taskSwitcher.position = 0
        }
        onPressed: managePressed(mouse);

        onPositionChanged: {
            if (!isDragging && Math.abs(startMouseY - oldMouseY) < root.height) {
                oldMouseY = mouse.y;
                return;
            } else {
                isDragging = true;
            }

            taskSwitcher.peeking = true;
            taskSwitcher.overShoot = -mouse.y/taskSwitcher.drawerHeight;
            taskSwitcher.position = taskSwitcher.overShoot;
            oldMouseY = mouse.y;

            if (taskSwitcher.visibility == Window.Hidden && taskSwitcher.position > 0) {
                taskSwitcher.showFullScreen();
            }
            if (!wasShowingDesktop) {
                plasmoid.nativeInterface.showDesktop = mouse.y < -plasmoid.availableScreenRect.height/2;
                // if showing desktop, retract the drawer
                if (plasmoid.nativeInterface.showDesktop) {
                    taskSwitcher.position = 1 + (mouse.y + plasmoid.availableScreenRect.height/2) / taskSwitcher.drawerHeight
                }
            }
        }
        onReleased: {
            if (!isDragging) {
                return;
            }
            taskSwitcher.overShoot = 0;
            taskSwitcher.peeking = false;

            if (taskSwitcher.visibility == Window.Hidden) {
                return;
            }
        }

        Rectangle {
            anchors.fill: parent
            color: root.backgroundColor

            visible: plasmoid.configuration.PanelButtonsVisible

            Button {
                anchors.left: parent.left
                height: parent.height
                width: parent.width/3
                enabled: taskSwitcher.tasksCount > 0;
                iconSource: "box"
                onClicked: {
                    plasmoid.nativeInterface.showDesktop = false;
                    taskSwitcher.visible ? taskSwitcher.close() : taskSwitcher.open();
                }
                onPressed: mainMouseArea.managePressed(mouse);
                onPositionChanged: mainMouseArea.positionChanged(mouse);
                onReleased: mainMouseArea.released(mouse);
            }

            Button {
                id: showDesktopButton
                height: parent.height
                width: parent.width/3
                anchors.horizontalCenter: parent.horizontalCenter
                iconSource: "start-here-kde"
                enabled: taskSwitcher.tasksCount > 0
                checkable: true
                onCheckedChanged: {
                    taskSwitcher.close();
                    plasmoid.nativeInterface.showDesktop = checked;
                }
                onPressed: mainMouseArea.managePressed(mouse);
                onPositionChanged: mainMouseArea.positionChanged(mouse);
                onReleased: mainMouseArea.released(mouse);
                Connections {
                    target: taskSwitcher
                    onCurrentTaskIndexChanged: {
                        if (taskSwitcher.currentTaskIndex < 0) {
                            showDesktopButton.checked = false;
                        }
                    }
                }
            }

            Button {
                height: parent.height
                width: parent.width/3
                anchors.right: parent.right
                iconSource: "paint-none"
                enabled: taskSwitcher.model.activeTask.valid && taskSwitcher.model.data(taskSwitcher.model.activeTask, TaskManager.AbstractTasksModel.IsClosable)
                onClicked: {
                    var index = taskSwitcher.model.activeTask;
                    if (index) {
                        taskSwitcher.model.requestClose(index);
                    }
                }
                onPressed: mainMouseArea.managePressed(mouse);
                onPositionChanged: mainMouseArea.positionChanged(mouse);
                onReleased: mainMouseArea.released(mouse);
            }
        }
    }
}
