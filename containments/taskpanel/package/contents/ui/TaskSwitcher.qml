/*
 *   Copyright 2015 Marco Martin <notmart@gmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import QtQuick.Controls 2.3 as Controls
import org.kde.taskmanager 0.1 as TaskManager
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.private.minishell 2.0 as MiniShell

MiniShell.FullScreenPanel {
    id: window

    visible: false
    width: Screen.width
    height: Screen.height
    acceptsFocus: false

    property alias position: drawer.position
    property real overShoot
    property bool peeking: false
    
    property int tasksCount: tasksModel.count
    property int currentTaskIndex: -1
    property alias model: tasksModel
    property alias drawerHeight: drawer.height

    Component.onCompleted: plasmoid.nativeInterface.panel = window;

    onTasksCountChanged: {
        if (tasksCount == 0) {
            close();
        }
    }
    color: Qt.rgba(0, 0, 0, 0.3 * position)

    function open() {
        taskSwitcher.showFullScreen();
        drawer.open()
    }
    function close() {
        drawer.close()
    }

    function setSingleActiveWindow(id) {
        if (id >= 0) {
            tasksModel.requestActivate(tasksModel.index(id, 0));
        }
    }

    onPeekingChanged:  {
        if (peeking) {
            drawer.enter.enabled = false;
            drawer.exit.enabled = false;
            drawer.visible = true;
        } else {
            positionResetAnim.to = position > 0.5 ? 1 : 0;
            positionResetAnim.running = true
            drawer.enter.enabled = true;
            drawer.exit.enabled = true;
        }
    }

    SequentialAnimation {
        id: positionResetAnim
        property alias to: internalAnim.to
        NumberAnimation {
            id: internalAnim
            target: drawer
            to: 0
            property: "position"
            duration: (drawer.position)*units.longDuration
        }
        ScriptAction {
            script: {
                if (internalAnim.to == 0) {
                    drawer.close();
                } else {
                    drawer.open();
                }
            }
        }
    }

    PlasmaComponents.Label {
        anchors.centerIn: parent
        font.pointSize: 24
        color: "white"
        text: i18n("Go To Homescreen")
        visible: !plasmoid.nativeInterface.showDesktop
        opacity: window.overShoot -1
    }
    TaskManager.TasksModel {
        id: tasksModel
    }
    Controls.Drawer {
        id: drawer
        edge: Qt.BottomEdge
        width: window.width
        height: units.gridUnit * 8
        //instead of onclised as this let's the animation run
        onVisibleChanged: {
            if (!visible) {
                window.visible = false;
            }
        }

        contentItem: Item {
            ListView {
                id: tasksView
                orientation: ListView.Horizontal
                anchors.fill: parent

                //This proxy is only used for "get"
                PlasmaCore.SortFilterModel {
                    id: filterModel
                    sourceModel: TaskManager.TasksModel {}
                    onCountChanged: {
                        if (count == 0) {
                            window.close();
                        }
                    }
                }

                model: tasksModel

                delegate: Task {}
                displaced: Transition {
                    NumberAnimation {
                        properties: "x,y"
                        duration: units.longDuration
                        easing.type: Easing.InOutQuad
                    }
                }
                remove: Transition {
                    ParallelAnimation {
                        OpacityAnimator {
                            to: 0
                            duration: units.longDuration
                            easing.type: Easing.InOutQuad
                        }
                        YAnimator {
                            to: -tasksView.height
                            duration: units.longDuration
                            easing.type: Easing.InOutQuad
                        }
                    }
                }
            }

            PlasmaComponents.Label {
                anchors.centerIn: parent
                font.pointSize: 24
                text: i18n("No applications running")
                visible: tasksView.count === 0
            }
        }
    }
}
