/*
 *  Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2 as Controls
import QtGraphicalEffects 1.0

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.kquickcontrolsaddons 2.0 as KQuickControlAddons
import org.kde.kirigami 2.5 as Kirigami
import org.kde.draganddrop 2.0 as DragDrop

import Mycroft 1.0 as Mycroft

import org.kde.plasma.private.containmentlayoutmanager 1.0 as ContainmentLayoutManager 

MouseArea {
    id: root
    width: 640
    height: 480

//BEGIN properties
    property Item toolBox
    readonly property bool smallScreenMode: Math.min(width, height) < Kirigami.Units.gridUnit * 18

//END properties

//BEGIN functions

//END functions

//BEGIN slots
    Component.onCompleted: {
        Mycroft.MycroftController.start();
    }

    Timer {
        interval: 10000
        running: Mycroft.MycroftController.status != Mycroft.MycroftController.Open
        onTriggered: {
            print("Trying to connect to Mycroft");
            Mycroft.MycroftController.start();
        }
    }

    Containment.onAppletAdded: {
        appletsSpace.addApplet(applet, x, y);
    }

    Connections {
        target: Mycroft.MycroftController
        onListeningChanged: {
            if (Mycroft.MycroftController.listening) {
                plasmoid.nativeInterface.showDesktop = true;
            }
        }
    }
//END slots

    PlasmaCore.ColorScope {
        id: initialScreen

        anchors.fill: parent
        colorGroup: PlasmaCore.Theme.ComplementaryColorGroup
        Kirigami.Theme.colorSet: Kirigami.Theme.Complementary


        DragDrop.DropArea {
            anchors.fill: parent

            onDragEnter: {
                event.accept(event.proposedAction);
            }
            onDragMove: {
                appletsLayout.showPlaceHolderAt(
                    Qt.rect(event.x - appletsLayout.minimumItemWidth / 2,
                    event.y - appletsLayout.minimumItemHeight / 2,
                    appletsLayout.minimumItemWidth,
                    appletsLayout.minimumItemHeight)
                );
            }

            onDragLeave: {
                appletsLayout.hidePlaceHolder();
            }

            preventStealing: true

            onDrop: {
                plasmoid.processMimeData(event.mimeData,
                            event.x - appletsLayout.placeHolder.width / 2, event.y - appletsLayout.placeHolder.height / 2);
                event.accept(event.proposedAction);
                appletsLayout.hidePlaceHolder();
            }

            ContainmentLayoutManager.AppletsLayout {
                id: appletsLayout
                anchors.fill: parent
                configKey: width > height ? "ItemGeometries" : "ItemGeometriesVertical"
                containment: plasmoid
                editModeCondition: plasmoid.immutable
                        ? ContainmentLayoutManager.AppletsLayout.Manual
                        : ContainmentLayoutManager.AppletsLayout.AfterPressAndHold

                // Sets the containment in edit mode when we go in edit mode as well
                onEditModeChanged: plasmoid.editMode = editMode

                minimumItemWidth: units.gridUnit * 3
                minimumItemHeight: minimumItemWidth

                cellWidth: units.iconSizes.small
                cellHeight: cellWidth

                acceptsAppletCallback: function(applet, x, y) {
                    return applet.pluginName !== "org.kde.mycroft.tablet.launcher";
                }

                onAppletRefused: {
                    if (applet.pluginName === "org.kde.mycroft.tablet.launcher") {
                        launcherContainer.applet = applet;
                    }
                }

                appletContainerComponent: ContainmentLayoutManager.AppletContainer {
                    id: appletContainer
                    readonly property bool isIcon: applet && applet.pluginName === "org.kde.plasma.icon"

                    editModeCondition: plasmoid.immutable
                        ? ContainmentLayoutManager.ItemContainer.Manual
                        : ContainmentLayoutManager.ItemContainer.AfterPressAndHold

                    Layout.preferredWidth: isIcon
                        ? units.iconSizes.large + units.gridUnit * 4
                        : ((applet && applet.Layout ? applet.Layout.preferredWidth : -1) + leftPadding + rightPadding)
                    Layout.preferredHeight: isIcon
                        ? units.iconSizes.large + units.gridUnit
                        : ((applet && applet.Layout ? applet.Layout.preferredHeight : -1) + topPadding + bottomPadding)

                    background: DropShadow {
                        visible: applet && applet.backgroundHints == PlasmaCore.Types.StandardBackground
                        anchors.fill: parent
                        horizontalOffset: 0
                        verticalOffset: 2
                        source: applet
                    }
                    configOverlayComponent: ConfigOverlay {}
                }

                placeHolder: ContainmentLayoutManager.PlaceHolder {}

                opacity: !skillView.open

                Behavior on opacity {
                    OpacityAnimator {
                        duration: Kirigami.Units.longDuration
                        easing.type: Easing.InOutQuad
                    }
                }
            }
/*
            ContainmentLayoutManager.AppletsArea {
                id: appletsSpace
                anchors.fill: parent

                cellWidth: units.iconSizes.large

                acceptsAppletCallback: function (applet, x, y) {
                    return applet.pluginName !== "org.kde.mycroft.tablet.launcher";
                }

                appletContainerComponent: ContainmentLayoutManager.AppletContainer {
                    readonly property bool isIcon: applet && applet.pluginName === "org.kde.plasma.icon"

                    Layout.preferredWidth: isIcon
                        ? units.iconSizes.large + units.gridUnit * 4
                        : ((applet && applet.Layout ? applet.Layout.preferredWidth : -1) + leftPadding + rightPadding)
                    Layout.preferredHeight: isIcon
                        ? units.iconSizes.large + units.gridUnit
                        : ((applet && applet.Layout ? applet.Layout.preferredHeight : -1) + topPadding + bottomPadding)

                    background: DropShadow {
                        visible: applet && applet.backgroundHints == PlasmaCore.Types.StandardBackground
                        anchors.fill: parent
                        horizontalOffset: 0
                        verticalOffset: 2
                        source: applet
                    }
                }

                onAppletRefused: {
                    if (applet.pluginName === "org.kde.mycroft.tablet.launcher") {
                        launcherContainer.applet = applet;
                    }
                }
            }*/
            Item {
                id: launcherContainer

                anchors {
                    bottom: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }

                z: 999
                property Item applet
                onAppletChanged: {
                    applet.parent = launcherContainer;
                    applet.anchors.fill = launcherContainer;
                    applet.visible = true;
                }
                visible: applet
                width: Math.min(units.gridUnit * 35, root.width)
                height: units.gridUnit * 6
            }
        }
    }

    Mycroft.SkillView {
        id: skillView
        anchors.fill: parent
        Kirigami.Theme.colorSet: Kirigami.Theme.Complementary
        open: false
        //FIXME: find a better way for timeouts
        //onActiveSkillClosed: open = false;
/*
        topPadding: plasmoid.availableScreenRect.y
        bottomPadding: root.height - plasmoid.availableScreenRect.y - plasmoid.availableScreenRect.height
        leftPadding: plasmoid.availableScreenRect.x
        rightPadding: root.width - plasmoid.availableScreenRect.x - plasmoid.availableScreenRect.width
        */
    }

    Controls.RoundButton {
        anchors {
            right: parent.right
            top: parent.top
            margins: Kirigami.Units.largeSpacing
            topMargin: Kirigami.Units.largeSpacing + plasmoid.availableScreenRect.y
        }
        width: Kirigami.Units.iconSizes.large
        height: width
        visible: skillView.currentItem
        icon.name: skillView.open ? "go-previous-symbolic" : ""
        Image {
            anchors.fill: parent
            visible: !skillView.open
            source: Qt.resolvedUrl("mycroft.png");
        }
        onClicked: skillView.open = !skillView.open;
    }

    Mycroft.StatusIndicator {
        anchors {
            right: parent.right
            top: parent.top
            margins: Kirigami.Units.largeSpacing
            topMargin: Kirigami.Units.largeSpacing + plasmoid.availableScreenRect.y
        }
    }
}
