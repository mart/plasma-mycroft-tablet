/*
 *  Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.1

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents

import org.kde.plasma.private.containmentlayoutmanager 1.0 as ContainmentLayoutManager

ContainmentLayoutManager.ConfigOverlayWithHandles {
    id: overlay

    RowLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        y: bottomAvailableSpace > height + units.gridUnit
            ? parent.height + units.gridUnit
            : -height - units.gridUnit

        width: parent.width * 0.6
        PlasmaComponents.Button {
            implicitHeight: units.iconSizes.medium
            implicitWidth: implicitHeight
            icon.name: "configure"
            onClicked: {
                appletContainer.applet.action("configure").trigger();
                appletContainer.editMode = false;
            }
        }
        Item {
            Layout.fillWidth: true
        }
        PlasmaComponents.Button {
            implicitHeight: units.iconSizes.medium
            implicitWidth: implicitHeight
            width: height
            icon.name: "delete"
            onClicked: {
                appletContainer.applet.action("remove").trigger();
                appletContainer.editMode = false;
            }
        }
    }
}

