/*
 *  Copyright 2015 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.3 as Controls
import QtQuick.Window 2.2
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kquickcontrolsaddons 2.0
import org.kde.plasma.private.minishell 2.0 as MiniShell

PlasmaCore.ColorScope {
    id: root
    width: 40 * units.gridUnit
    height: 6 * units.gridUnit
    Plasmoid.backgroundHints: "NoBackground";

//BEGIN properties
    property int cellHeight: root.height
    property int cellWidth: cellHeight - units.gridUnit * 2
//END properties

//BEGIN slots
    Component.onCompleted: {
        plasmoid.nativeInterface.applicationListModel.appOrder = plasmoid.configuration.AppOrder;
        plasmoid.nativeInterface.applicationListModel.loadApplications();
    }

    Connections {
        target: plasmoid.nativeInterface.applicationListModel
        onAppOrderChanged: {
            plasmoid.configuration.AppOrder = plasmoid.nativeInterface.applicationListModel.appOrder;
        }
    }
//END slots

    SequentialAnimation {
        id: clickFedbackAnimation
        property Item target
        NumberAnimation {
            target: clickFedbackAnimation.target
            properties: "scale"
            to: 2
            duration: units.longDuration
            easing.type: Easing.InOutQuad
        }
        PauseAnimation {
            duration: units.shortDuration
        }
        NumberAnimation {
            target: clickFedbackAnimation.target
            properties: "scale"
            to: 1
            duration: units.longDuration
            easing.type: Easing.InOutQuad
        }
    }
    FeedbackWindow {
        id: feedbackWindow
    }

    clip: -launcherScroller.contentY >= launcherScroller.topMargin
    LauncherScroller {
        id: launcherScroller
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }

        leftPadding: Math.max(0, width - parent.width) / 2
        rightPadding: leftPadding
        width: Screen.width
        height: Screen.height

        availableCellHeight: root.height
        topMargin: Screen.height - availableCellHeight
        Connections {
            target: Screen
            onHeightChanged: closeAnim.restart();
        }

        onExternalDragStarted: closeAnim.restart();

        onMovementEnded: {
            if (contentY >= 0) {
                return;
            }

            if (-contentY < Screen.height/2
                || (launcherScroller.contentHeight - launcherScroller.height - launcherScroller.contentY < launcherScroller.contentHeight / 2)) {
                openAnim.restart();
            } else {
                closeAnim.restart();
            }
        }

        NumberAnimation {
            id: openAnim
            target: launcherScroller
            property: "contentY"
            from: launcherScroller.contentY
            to: Math.min(
                    -units.gridUnit * 3,
                    launcherScroller.contentHeight - launcherScroller.height)
            duration: units.longDuration
            easing.type: Easing.InOutQuad
        }


        NumberAnimation {
            id: closeAnim
            target: launcherScroller
            property: "contentY"
            from: launcherScroller.contentY
            to: -launcherScroller.topMargin
            duration: units.longDuration
            easing.type: Easing.InOutQuad
        }
    }
}
